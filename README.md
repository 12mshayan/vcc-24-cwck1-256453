# VCC-24-CwCk1-256453



## Getting started

This project implements a a 3-tier, container-based solution designed to support the promotional activity for GlynH’s Crisps which is UK based company that assists the launch of a new F1 (Formula One) themed energy snack. The launch will coincide with the start of the ’24 F1 season, with GlynH’s Crisps printing special promotional bags that advertise a URL to a free prize draw. It consists of a React-based frontend, a Node.js backend, and a MySQL database, all containerized using Docker.



## Prerequisites

- Docker
- Docker Compose
- Git (for version control)
- React
- Node
- EXPRESS
- MySQL







## Installation & Running
Clone the repository to your local machine using:



- [ ] [Set up project integrations](git clone <https://gitlab.com/12mshayan/vcc-24-cwck1-256453.git>
)


## To start the application, ensure Docker is running, then execute:

- docker-compose up --build

This command builds the images if they are not already created and starts the containers as specified in the docker-compose.yml file. The --build option ensures that any changes to the Dockerfiles or build contexts are considered.


# Testing

Testing is automated using a single command. To run all tests, execute:

- docker-compose exec api npm test

Ensure that the api service container is running before executing the test command. This command runs the test suite defined in the backend's package.json.


## Architecture

**Services**
- Frontend (web): Serves the user interface on port 3000. Built using React.
- API (api): Handles business logic and data manipulation, accessible on port 3001. Built with Node.js.
- Database (db): Stores all application data, using MySQL database on port 3306.

**Configuration**
- docker-compose.yml: Defines the setup for the services, including dependencies, port mappings, and environment variables.
- Dockerfile: Exists in both the frontend and backend directories, defining how the Docker images for these services are built.




## Functioning Components

- User interface for monitoring and controlling the environment.
- Backend API for data processing and storage.
- Real-time data synchronization between the frontend and backend.
- Data persistence using MySQL database.

## Development and Watch Mode

Development is facilitated using Docker Compose's watch mode, which monitors changes in the file system:

- Frontend: Any changes in package.json or the app directory trigger a rebuild or sync, respectively.
- Backend: Similar to the frontend, with changes in the Node.js application resulting in immediate updates without rebuilding the container.

## Logs and Monitoring

- docker-compose logs -f

## Cleanup

To stop and remove all running containers associated with this compose file, run:

- docker-compose down


