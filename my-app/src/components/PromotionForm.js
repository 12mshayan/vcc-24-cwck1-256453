import React, { useState } from 'react';
import axios from 'axios';

function PromotionForm() {
    const [formData, setFormData] = useState({
        hexCode: '',
        name: '',
        email: '',
        address: ''
    });

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            // Include formData as the payload for the POST request
            const response = await axios.post('http://localhost:3001/submit', formData);
            console.log(response.data.message); // Correctly access the message from response.data
            alert(`Voucher code: ${response.data.message}`); // Correct this line to use response.data.message
        } catch (error) {
            if (error.response && error.response.data && error.response.data.message) {
                alert(error.response.data.message); // More safely access error response data
            } else {
                alert('An unknown error occurred');
            }
        }
    };
    
    

    return (
        <form onSubmit={handleSubmit}>
            <input type="text" name="hexCode" placeholder="10-digit hex code" onChange={handleChange}  />
            <input type="text" name="name" placeholder="Name" onChange={handleChange}  />
                <input type="email" name="email" placeholder="Email" onChange={handleChange}  />
                <input type="text" name="address" placeholder="Address" onChange={handleChange}  />
                <button type="submit">Submit</button>
        </form>
    );
}

export default PromotionForm;

