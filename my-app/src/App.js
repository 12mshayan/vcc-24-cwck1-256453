import React from 'react';
import PromotionForm from './components/PromotionForm'; 
import './App.css'; 

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>GlynH's Crisps Promotion</h1>
      </header>
      <main>
        <PromotionForm />
      </main>
    </div>
  );
}

export default App;
