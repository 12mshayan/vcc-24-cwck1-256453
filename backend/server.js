const express = require('express');
const bodyParser = require('body-parser');
const sql = require('mssql');
const cors = require('cors');

const app = express();
app.use(bodyParser.json());
app.use(cors()); // Enable CORS for all routes

const dbConfig = {
    server: 'sqldatabasecloud.database.windows.net',
    database: 'MyDbCloud',
    user: 'MShayan',
    password: 'Maryam12345',
    options: {
        encrypt: true,
        trustServerCertificate: false
    },
    pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 30000
    }
};

app.post('/submit', async (req, res) => {
    const { hexCode, name, email, address } = req.body;

    // Validate the hexCode
    if (!/^[0-9A-Fa-f]{10}$/.test(hexCode)) {
        return res.status(400).json({ message: 'Invalid Hex Code' });
    }

    try {
        await sql.connect(dbConfig);
        const pool = new sql.Request();

        // Check if hexCode has already been used
        const { recordset: existingEntries } = await pool.query(`SELECT * FROM dbo.GlynH WHERE hexCode = '${hexCode}'`);

        if (existingEntries.length > 0) {
            // If hexCode exists, update name, email, and address for this hexCode
            await pool.query(`UPDATE dbo.GlynH SET name='${name}', email='${email}', address='${address}' WHERE hexCode='${hexCode}'`);
        } else {
            // Decide voucher type (vType) and code (vCode) for a new hexCode
            const isUmbrella = Math.random() < 0.01; // 1 in 100 chance
            const vType = isUmbrella ? 'Umbrella' : 'Discount';
            const vCode = isUmbrella ? 'FREEUMBRELLA' : '5OFF';

            // Insert new entry if hexCode does not exist
            await pool.query(`INSERT INTO dbo.GlynH (hexCode, name, email, address, vType, vCode) VALUES ('${hexCode}', '${name}', '${email}', '${address}', '${vType}', '${vCode}')`);
        }

        // Fetch and return the vCode for the hexCode
        const { recordset } = await pool.query(`SELECT vCode FROM dbo.GlynH WHERE hexCode = '${hexCode}'`);
        const vCode = recordset[0].vCode;

        await sql.close();

        res.json({ vCode: vCode, message: `Successfully registered. Your voucher code: ${vCode}` });
    } catch (err) {
        console.error('Database error:', err);
        res.status(500).json({ message: 'Internal Server Error', error: err.message });
    }
});

app.listen(3001, () => {
    console.log('Server running on port 3001');
});
